import { Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Product(props){
    const { breakPoint, data } = props

    const { _id, name, description, regularPrice } = data

    return(
        <Col xs={12} md={breakPoint}>
            <Card className="cardHighlight mx-2">
                <Card.Body>
                    <Card.Title className="text-center">
                        <Link to={`/product/getProduct/${  _id}`}>{name}</Link>
                        {name}
                    </Card.Title>
                    <Card.Text>{description}</Card.Text>
                    
                </Card.Body>
                <Card.Footer>
                    <h5 className="text-center">₱{regularPrice}</h5>
                    <Link className="btn btn-primary d-block" to={`/product/getProduct/${  _id}`}>Details</Link>
                </Card.Footer>
            </Card>
        </Col>
    )
}
