import React, { useState, useEffect } from 'react';
import { CardGroup, Card, Button } from 'react-bootstrap';
import PreviewProduct from './PreviewProduct';

export default function FeaturedCourses() {
  const [previews, setPreviews] = useState([]);

  useEffect(() => {
    fetch(`https://backendforcapstone3.onrender.com/product/getAllproducts`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setPreviews(data); // Set the retrieved data to the state.
      });
  }, []);

  return (
    <>
      <h2 className="text-center">Featured Products</h2>
      <CardGroup className="justify-content-center">
        {previews.map((product, index) => (
          <Card key={index} style={{ width: '18rem' }}>
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Text>{product.description}</Card.Text>
              <Card.Text>Regular Price: {product.regularPrice}</Card.Text>
              <Card.Text>Original Price: {product.originalPrice}</Card.Text>

             
            </Card.Body>
          </Card>
        ))}
      </CardGroup>
    </>
  );
}
