import { Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({ product }) {
  // State for product id, which will be used for fetching
  const [productId, setProductId] = useState('');

  // State for edit product modal
  const [showEdit, setShowEdit] = useState(false);

  // State for form fields
  const [name, setName] = useState(product.name);
  const [description, setDescription] = useState(product.description);
  const [category, setCategory] = useState(product.category);
  const [brand, setBrand] = useState(product.brand);
  const [quantity, setQuantity] = useState(product.quantity);
  const [originalPrice, setOriginalPrice] = useState(product.originalPrice);
  const [regularPrice, setRegularPrice] = useState(product.regularPrice);

  // Function for opening the edit modal
  const openEdit = (productId) => {
    setShowEdit(true);

    // To still get the actual data from the form
    fetch(`https://backendforcapstone3.onrender.com/product/getProduct/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductId(data._id);
        setName(data.name);
        setDescription(data.description);
        setCategory(data.category);
        setBrand(data.brand);
        setQuantity(data.quantity);
        setOriginalPrice(data.originalPrice);
        setRegularPrice(data.regularPrice);
      });
  }

  const closeEdit = () => {
    setShowEdit(false);
    // Reset form fields
    setName(product.name);
    setDescription(product.description);
    setCategory(product.category);
    setBrand(product.brand);
    setQuantity(product.quantity);
    setOriginalPrice(product.originalPrice);
    setRegularPrice(product.regularPrice);
  }

  // Function to save the update
  const editProduct = (e, productId) => {
    e.preventDefault();

    console.log(productId);
    console.log(name);
    console.log(description);
    console.log(category);
    console.log(brand);
    console.log(quantity);
    console.log(originalPrice);
    console.log(regularPrice);

    fetch(`https://backendforcapstone3.onrender.com/product/updateProduct/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        category: category,
        brand: brand,
        quantity: quantity,
        originalPrice: originalPrice,
        regularPrice: regularPrice,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Update Success!',
            icon: 'success',
            text: 'Product Successfully Updated!',
          });

          closeEdit();
        } else {
          Swal.fire({
            title: 'Update Error!',
            icon: 'error',
            text: 'Please try again!',
          });
          closeEdit();
        }
      });
  }

  return (
    <>
      <Button variant="primary" size="sm" onClick={() => { openEdit(product) }}>Edit</Button>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e, product)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control type="text" required value={name} onChange={(e) => setName(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control type="text" required value={description} onChange={(e) => setDescription(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productCategory">
              <Form.Label>Category</Form.Label>
              <Form.Control type="text" required value={category} onChange={(e) => setCategory(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productBrand">
              <Form.Label>Brand</Form.Label>
              <Form.Control type="text" required value={brand} onChange={(e) => setBrand(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productQuantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control type="number" required value={quantity} onChange={(e) => setQuantity(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productOriginalPrice">
              <Form.Label>Original Price</Form.Label>
              <Form.Control type="number" required value={originalPrice} onChange={(e) => setOriginalPrice(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productRegularPrice">
              <Form.Label>Regular Price</Form.Label>
              <Form.Control type="number" required value={regularPrice} onChange={(e) => setRegularPrice(e.target.value)} />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>Close</Button>
            <Button variant="success" type="submit">Submit</Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
