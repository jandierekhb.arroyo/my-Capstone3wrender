import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import CreateProduct from './pages/CreateProduct';
import Profile from './pages/Profile';
import './App.css';
import { UserProvider } from './UserContext';
import FeaturedProduct from './components/FeaturedProduct'
import { Container } from 'react-bootstrap';
import AdminTable from './pages/AdminTable';


function App() {

    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

    // Function for clearing localStorage on logout
    const unsetUser = () => {

      localStorage.clear();

    };


    useEffect(() => {

    // console.log(user);
    fetch(`https://backendforcapstone3.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

      // Else set the user states to the initial values
      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

    }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/product/allProducts" element={<FeaturedProduct />} />

            <Route path="/search" element={<Products />} />

            <Route path="/adminTable" element={<AdminTable />} />
          
           
            <Route path="/product/getProduct/:productId" element={<ProductView />}/>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/createProduct" element={<CreateProduct />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="*" element={<Error />} />

          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;