import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import ProductCard from '../components/ProductCard';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "Administrator Panel",
        content: "Admin for Admin users only",
        /* buttons */
        destination: "/products",
        label: "Check now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}