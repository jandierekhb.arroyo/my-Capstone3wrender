import { useEffect,useState } from "react"
import AdminView from "./AdminView";

export default function AdminTable(){

    const [productsData, setProductsData] = useState([]);

    const fetchData = () => {
        fetch(`https://backendforcapstone3.onrender.com/product/getAllproducts`)
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setProductsData(data); 
        });
    }

    useEffect(() => {
        fetchData();

      }, []);


    
    return ( 
        <>
        <AdminView productsData={productsData} fetchData={fetchData} />
        </>
    )
}